import bpy
import asyncio
import argparse
import sys
import logging

logger = logging.getLogger("blender_debug")
logger.setLevel(logging.DEBUG)

DEFAULT_PTVSD_PORT = 5688


def parse():
    args_ = []
    copy_arg = False
    for arg in sys.argv:
        if arg == "--":
            copy_arg = True
        elif copy_arg:
            args_.append(arg)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--ptvsd", type=int, default=DEFAULT_PTVSD_PORT, help="Vscode debugger port"
    )
    parser.add_argument(
        "--wait_for_debugger",
        default=False,
        action="store_true",
        help="wait for debugger",
    )
    args, _ = parser.parse_known_args(args_)
    return args


class WaitDebugguerOperator(bpy.types.Operator):
    bl_idname = "blender_debug.wait_for_debugguer"
    bl_label = "Wait for python debugguer"
    bl_options = {"REGISTER"}

    def execute(self, context):
        import ptvsd

        ptvsd.wait_for_attach()

        return {"FINISHED"}


class DebugPanel(bpy.types.Panel):
    """Panel"""

    bl_label = "Debug"
    bl_idname = "DEBUG_PT_settings"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Debug"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.operator(WaitDebugguerOperator.bl_idname, text="Wait for Debugguer")


classes = (
    WaitDebugguerOperator,
    DebugPanel,
)


def register():
    for c in classes:
        bpy.utils.register_class(c)


def main(ptvsd_port=DEFAULT_PTVSD_PORT, wait_for_debugger=False):
    try:
        import ptvsd
    except ImportError:
        import subprocess

        subprocess.run([bpy.app.binary_path_python, "-m", "pip", "install", "ptvsd"])

    try:
        import ptvsd

        ptvsd.enable_attach(address=("localhost", ptvsd_port), redirect_output=True)
        if wait_for_debugger:
            ptvsd.wait_for_attach()
    except ImportError:
        logger.error("Unable to run ptvsd")
        return

    logger.info("Starting:")
    logger.info("  ptvsd  port %s", ptvsd_port)

    register()


if __name__ == "__main__":
    args = parse()
    main(args.ptvsd, args.wait_for_debugger)
